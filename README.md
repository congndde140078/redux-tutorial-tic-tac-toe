# Redux Tutorial: Tic-Tac-Toe

This is a simple app to play a game of tic-tac-toe, written to use Redux.js and React.js. It's intended to serve as a concise example of what a React+Redux app looks like. You can either clone this repo and use the app yourself (see next section), or try following along with each step of the development for a step-by-step tutorial (see [Tutorial](#tutorial) below).

### Usage

- Clone this repo and `cd` to the directory.
- Run `npm install`
- Run `npm run watch` if you plan to make changes to the source code (in which case the site will be rebuilt automatically), or `npm run build` otherwise.
- Start an HTTP server in the `dist/` directory (i.e. the one containing `index.html`). If you don't know how to do that, see [these instructions](https://superuser.com/q/231080).
- Open http://localhost:8080 (or whatever port your server is listening on) in your browser.

### Tutorial

I ([Jeff Terrell](https://terrell.web.unc.edu)) gave a lecture introducing Redux.js to the COMP 523 (software engineering laboratory) class on Wednesday, January 30, 2019, with essentially this code. This repo is an artifact of that lecture. [As seen elsewhere](https://gitlab.com/unc-app-lab/react-native-tutorial-tic-tac-toe), the following is a list of the small steps I took to introduce key concepts. You can check out the code for a step to see and run the code at that step. (The step points to the commit at the completion of that step.)

Why Redux? Because I believe Redux apps are simpler, and that simplicity makes it easier to create robust apps without bugs. It's my opinion that most of the bugs and challenges in programming are incidental, based on the choice of language and approach, rather than essential and inherent in the problem being solved, and that most of that incidental complexity stems from an uncareful approach to effects: the parts of your code that actually change some state somewhere. Indeed, the object oriented style favored by many is usually (though not always) uncareful in exactly this way: effects constitute a rather large proportion of the total lines of code. In Redux, there is exactly one way to create an effect: by dispatching one of a predefined set of actions against the state stored by Redux. This careful isolation of effects into a single spot, and a commensurate emphasis on pure functions (those without effect or external reference) tend to make apps written in Redux simple and robust.

- [`step01`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step01): initialize the project (by running `npm init -y` in a new directory named `redux-s19` and removing the line specifying a main entry point)
  - This creates a `package.json` file describing a new node project.
  - Node.js is a way to run javascript code outside of a web browser.
  - This enables a more organized approach to a software project, with proper dependencies, modules, etc.
- [`step02`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step02): add tooling
  - Although I tend to dislike configuring tools, it is necessary. We need tools to:
    - combine the code from all our modules into a single script to be included in an HTML page
    - similarly combine the code from all our dependencies
    - translate newer Javascript features or syntax (like arrow functions, the object spread operator, or JSX syntax) into something browsers understand
  - The basic idea is to store source code modules in `src/`, a base HTML file and CSS file in `dist/`, and tell the tools to compile all the Javascript to `dist/main.js`, which is included in the HTML file.
  - The commit message explains a bit about what each of the dependencies do.
- [`step03`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step03): state
  - If you're creating an app, I recommend as a first step sketching the set of screens your app will have when it's finished. This gives a clear target for what you're developing and will inform how you design your state.
  - Redux describes itself as "a state container". You need to design what the state for your app looks like.
  - The idea is that every change that happens in the display of your app should be driven by a corresponding change in your application's state.
  - In Redux, the state is a single data structure, typically a Javascript object.
- [`step04`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step04): actions
  - The second key concept in Redux is actions.
  - An action is a specific thing that can happen in your app.
  - These represent the transitions between one state of your app and the next.
  - Most of the time (but not always), the user initiates an action.
  - Actions can be parameterized, so that events of the same general type can be treated as a single type of action in Redux.
  - If you're designing an app, sketching a series of screens, defining a state structure, and listing all the actions and parameters of your app will provide a solid definition of what your app does. At that point, most of the rest of the coding is pretty straightforward.
- [`step05`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step05): view, functional components
  - Because Redux contains (all of) the state of the app, we no longer need class-based stateful React components and can use simpler functional components instead.
  - We use most of the same React components and style as shown in the previous lecture, except the top-level App component is functional now.
  - At this point, we can see a Tic-Tac-Toe grid on the page, but we can't change it (unless we change the initial state and reload).
- [`step06`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step06): store, reducer signature, effects, dispatch
  - We finally include Redux in our project and start to use it here.
  - Redux is a "state container". The store is that container.
  - The store, once created, has two methods of interest:
    - `dispatch` accepts an action as an argument and affects the store based on that action. Reducers specify the transformation that should occur for each kind of action; we'll see them next.
    - `getState` fetches the state from the store.
  - So far, we can dispatch actions, but no change occurs because we haven't defined any actual reducers, just a trivial one that doesn't actually transform the state.
  - Note the signature of the reducer: it is a function that accepts the current state and an action and should return a new, transformed state.
- [`step07`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step07): react-redux, connect
  - `react-redux` is a bridge to connect React components to the Redux store.
  - You wrap your top-level React component in a &lt;Provider&gt;, giving the store as a prop, which will establish a "context" in React to make the store implicitly available to all descendants of the Provider.
  - Components can access this store by "connecting" to it using the provided `connect` function.
  - This function will extract props from the state and from the dispatch function and pass those props to an inner component. It takes two arguments:
    - `mapStateToProps` is a function you define that accepts the current state and returns an object of props to pass on to the inner component. These props are usually "output" props in the sense that they are helping to display pieces of the state on screen.
    - `mapDispatchToProps` is a function you define that accepts the store's dispatch function and returns an object of props to pass to the inner component. These props are usually "input" props in the sense that they are providing a mechanism by which users can affect the state.
- [`step08`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step08): reducers, testability
  - Here we add our first "real" reducer to handle the move action (our only one so far).
  - Remember the reducer signature: a reducer is a function that takes the current state and an action as arguments and returns the new state.
  - Be careful not to actually modify the input state, or Redux and React will get confused.
  - There are various ways to make a copy of the input state without affecting it:
    - The most basic way is to use the Object.assign method, e.g. `Object.assign({}, state, {property: newValue})`
    - A newish feature in Javascript is the "object spread operator", which can do the same thing: `{...state, property: newValue}`
    - I prefer using [Ramda.js](https://ramdajs.com) (especially the [assocPath function](https://ramdajs.com/docs/#assocPath)), a very nice functional programming library for Javascript.
- [`step09`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step09): new game action/reducer/button
  - Here we add a new action along with its reducer and React markup. Easy.
- [`step10`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step10): async actions, middleware
  - So far, all of our actions are "synchronous": when they fire, they stop the world to change the state, at which point they're done.
  - There's another important class of actions that are "asynchronous". A network request is a good example. When you fire off a network request, you don't want to stop the world and block further input until the network request completes---that might take many seconds.
  - This step illustrates how to do an asynchronous action to fetch something from the network using the redux-thunk middleware. For more information, see [the Redux documentation](https://redux.js.org/advanced/async-actions).
- [`step11`](https://gitlab.com/unc-app-lab/redux-tutorial-tic-tac-toe/tree/step11): testing
  - add some tests using jest

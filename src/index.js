import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { App } from './views.jsx'
import { initialState, rootReducer } from './reducers.js'

const loggerMiddleware = createLogger()
const enhancers = applyMiddleware(thunkMiddleware, loggerMiddleware)
let store = createStore(rootReducer, initialState, enhancers)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

import fetch from 'cross-fetch'

export const Actions = {
  move: 'ACTION_MOVE',
  newGame: 'ACTION_NEW_GAME',
  fetchTodoStart: 'ACTION_FETCH_TODO_START',
  fetchTodoSuccess: 'ACTION_FETCH_TODO_SUCCESS',
  fetchTodoFailure: 'ACTION_FETCH_TODO_FAILURE',
}

export const move = (index) => ({
  type: Actions.move,
  payload: {index},
})

export const newGame = () => ({
  type: Actions.newGame,
})

export const fetchTodoStart = () => ({
  type: Actions.fetchTodoStart,
})

export const fetchTodoSuccess = (payload) => ({
  type: Actions.fetchTodoSuccess,
  payload
})

export const fetchTodoFailure = (error) => ({
  type: Actions.fetchTodoFailure,
  error
})

export const fetchTodo = () => (dispatch) => {
  const id = Math.floor(Math.random() * 200) + 1
  dispatch(fetchTodoStart())
  return fetch('https://jsonplaceholder.typicode.com/todos/' + id)
    .then(response => response.text())
    .then(json => dispatch(fetchTodoSuccess(json)))
    .catch(error => dispatch(fetchTodoFailure(error)))
}

import { initialState, rootReducer } from './reducers.js'
import { move, newGame } from './actions.js'

describe('rootReducer', () => {
  describe('when given a newGame action', () => {
    const action = newGame()
    const state = rootReducer(initialState, action)

    it('returns a state object', () => {
      expect(state).toBeInstanceOf(Object)
    })

    it('returns an array of squares representing a new board', () => {
      expect(state).toHaveProperty('squares')
      const { squares } = state
      expect(squares).toBeInstanceOf(Array)
      expect(squares).toHaveLength(9)
    })

    it('sets the turn to X', () => {
      expect(state).toHaveProperty('xIsNext')
      const { xIsNext } = state
      expect(xIsNext).toBe(true)
    })
  })

  describe('when given a move action', () => {
    it('can set the indicated square to X', () => {
      const index = 4
      const action = move(index)
      const state = rootReducer(initialState, action)
      expect(state.squares[index]).toEqual('X')
    })

    it('can set the indicated square to O', () => {
      const index = 4
      const state = { ...initialState, xIsNext: false }
      const action = move(index)
      const newState = rootReducer(state, action)
      expect(newState.squares[index]).toEqual('O')
    })

    it('switches whose turn it is', () => {
      const action = move(4)
      const state = rootReducer(initialState, action)
      expect(initialState.xIsNext).toBe(true)
      expect(state.xIsNext).toBe(false)
    })

    it('will not change a square that is already set', () => {
      let squares = initialState.squares.slice()
      const index = 4
      squares[index] = 'O'
      const action = move(index)
      const state = { ...initialState, squares }
      const newState = rootReducer(state, action)
      expect(newState.squares[index]).toEqual('O')
    })

    it('will not change a square if there is already a winner', () => {
      let squares = initialState.squares.slice()
      squares[0] = 'O'
      squares[1] = 'O'
      squares[2] = 'O'
      const index = 4
      const action = move(index)
      const state = { ...initialState, squares }
      const newState = rootReducer(state, action)
      expect(newState.squares[index]).toBeNull()
    })
  })
})

import { winner } from './util.js'

const makeSquares = (str) => str.split('').map(s => s === ' ' ? null : s)

describe('winner', () => {
  it('finds a horizontal winner', () => {
    expect(winner(makeSquares('XXX      '))).toEqual('X')
  })

  it('finds a vertical winner', () => {
    expect(winner(makeSquares('X  X  X  '))).toEqual('X')
  })

  it('finds a diagonal winner', () => {
    expect(winner(makeSquares('X   X   X'))).toEqual('X')
  })

  it('finds a winner with Xs or Os', () => {
    expect(winner(makeSquares('XXX      '))).toEqual('X')
    expect(winner(makeSquares('OOO      '))).toEqual('O')
  })

  it('returns undefined when there is not a winner yet', () => {
    expect(winner(makeSquares('X  O     '))).toBe(undefined)
  })

  it('returns null when there is a tie game', () => {
    expect(winner(makeSquares('XOXOXOOXO'))).toBe(null)
  })
})
